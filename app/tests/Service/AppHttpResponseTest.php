<?php declare(strict_types=1);

namespace App\Tests\Service;

use App\Entity\HttpInfo;
use App\Service\AppHttpResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

/**
 * Class AppHttpResponseTest
 * @package App\Tests\Service
 */
class AppHttpResponseTest extends TestCase
{
    /** @var AppHttpResponse $obj */
    private $obj;

    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        $this->obj = new AppHttpResponse( new HttpInfo() );
    }

    public function test__construct()
    {
        $appInfo = new HttpInfo();
        $appInfo->setMessage( 'info' )
            ->setStatus( 'status' );

        $obj = new AppHttpResponse( $appInfo );

        $this->assertTrue( ( $obj->getEntity()->getMessage() === 'info' )  );
        $this->assertTrue( ( $obj->getEntity()->getStatus() === 'status' )  );
    }

    public function testSetMessage()
    {
        $this->obj->setMessage( 'message' );
        $this->assertTrue( ( $this->obj->getEntity()->getMessage() === 'message' )  );

    }

    public function testSetStatus()
    {
        $this->obj->setStatus( 'status' );
        $this->assertTrue( ( $this->obj->getEntity()->getStatus() === 'status' )  );
    }
}
