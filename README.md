# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* in-between save (parking) person data between two system that do not communicate direct  
* get person data via rest
* give person data via rest (until now, only all data exported)

### What is next? ###

* getting specfic data
* Authentication (JWS maybe)
* Client-Handling-Integration
* Update Persons Data  
* Tests
* Translations 

### How to set up? ###

* clone project 
* $ docker-compose up -d
* $ cd app
* copy .env to .env.local  ( $ cp .env .env.local )
* edit .env.local -> uncomment the last row which begins with # DATABASE_URL
* $ docker-compose run php ./bin/console doctrine:schema:update --force
* in browser call http://localhost/persons (well, nothing to see)
* for post, use http-request.http in PHPStorm
* get the posted data, call http://localhost/persons?identifier=unique-for-client&client=the%204711& in browser 
* or the "get" from http-request.http in PHPStorm
* run PHPUnit-Tests
    * $ docker-compose exec php bin/phpunit tests
  
      (all test ...) OR
    * $ docker-compose run composer bin/phpunit
      
  
### something interessed; do not forget ###
* curl -X GET -i http://localhost/persons/all -d '{"identifier": "unique-for-client","client": "the 4711"}' | more
    * -X GET for Get-Method
    * -d '{json message body content}'


### Steps to reproduced ###
* $ docker-compose up -d
* $ docker run --rm --interactive --tty --volume $PWD:/app composer create-project symfony/skeleton app
* $ docker run --rm --interactive --tty --volume $PWD:/app composer require symfony/security-bundle
* $ docker run --rm --interactive --tty --volume $PWD:/app composer require doctrine/orm
* $ docker run --rm --interactive --tty --volume $PWD:/app composer require symfony/serializer 
* $ docker run --rm --interactive --tty --volume $PWD:/app composer require validator
* $ docker-compose run composer composer require symfony/monolog-bundle
* $ docker-compose run composer composer require --dev symfony/phpunit-bridge

### PHPUnit ###
docker-compose run composer bin/console debug:container --env-vars --env=test
docker-compose exec php bin/console debug:container --env-vars --env=test

$ composer dump-env prod    // creates .env.local.php



php bin/console debug:container --env-vars

php bin/console debug:container --env-vars foo


php bin/console debug:container --env-var=FOO

php bin/console debug:container --parameters
### PHPUnit ###
docker-compose run composer bin/console debug:container --env-vars --env=test
docker-compose exec php bin/console debug:container --env-vars --env=test

$ composer dump-env prod    // creates .env.local.php




