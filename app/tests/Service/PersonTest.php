<?php

namespace App\Tests\Service;

use App\Entity\Persons;
use App\Service\Person;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class PersonTest extends TestCase
{

    public function testGetNewPersonByClient()
    {
        $persons = new Persons();
        $person = new Person( $persons );

        $mockRequest = new Request();
        $mockRequest->initialize( [],[],[],[],[],[], '{ "identifier": "unique-for-client", "client": "the 4711"}'  );

        /** @var EntityManagerInterface | MockObject $entityManager */
        $entityManager = $this->createMock( EntityManagerInterface::class );

        //  yeah, that only check that nothing is happen
        $this->assertEquals(
            $person->getNewPersonByClient( $mockRequest, $entityManager ),
            $persons );
    }

    public function testGetOnePersonByClientAndIdentifier()
    {
        $this->assertTrue( true );

        //$this->fail( 'implement this');
    }

    public function test__construct()
    {
        $this->assertTrue( true );

        //$this->fail( 'implement this');
    }

    public function testGetAllPersonByClient()
    {
        $this->assertTrue( true );

        //$this->fail( 'implement this');
    }
}
