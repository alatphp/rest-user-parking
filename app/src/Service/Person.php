<?php declare(strict_types=1);

namespace App\Service;

use App\Entity\Persons;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use stdClass;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use function json_decode;
use function json_encode;

/**
 * Class Person
 * @package App\Service
 */
class Person
{
    /** @var Persons  */
    private $persons;
    /** @var \App\Entity\Person  */
    private $person;

    /**
     * Person constructor.
     * @param Persons $persons
     */
    public function __construct( Persons $persons )
    {
        $this->persons = $persons;
        $this->person = new \App\Entity\Person();
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Persons
     */
    public function getNewPersonByClient( Request $request,
                                          EntityManagerInterface $entityManager ): Persons
    {


        return $this->persons;
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @return Persons
     * @throws Exception
     */
    public function getAllPersonByClient( Request $request,
                                          EntityManagerInterface $entityManager,
                                          ValidatorInterface $validator ): Persons
    {
        try {
            $this->person->setClient( $request->toArray()['client'] );

            //  yes, it should be one error because type json or null is no error
            if ( 1 === $validator->validate( $this->person )->count() ) {
                $criteria = new stdClass();
                $criteria->client = $this->person->getClient();
                $criteria = json_decode( json_encode( $criteria ), true );

                $result = $entityManager->getRepository( \App\Entity\Person::class )->findBy( $criteria );
            }
            else {
                $result = [$this->person];
            }
        }
        catch ( Exception $exception ) {
            throw $exception;
        }

        $this->persons->setPersons( $result );

        return $this->persons;
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @return Persons
     * @throws Exception
     */
    public function getOnePersonByClientAndIdentifier(Request $request,
                                                      EntityManagerInterface $entityManager,
                                                      ValidatorInterface $validator
    ): Persons
    {
        try {
            $this->person->setIdentifier( $request->get('identifier' ) ?? $request->toArray()['identifier'] );
            $this->person->setClient( $request->get( 'client' ) ?? $request->toArray()['client'] );

            if ( 0 === $validator->validate( $this->person )->count() ) {
                $criteria = new stdClass();

                $criteria->client = $this->person->getClient();
                $criteria->identifier = $this->person->getIdentifier();

                $criteria = json_decode( json_encode( $criteria ), true );

                $result = $entityManager->getRepository( \App\Entity\Person::class )->findOneBy( $criteria );
            }
            else {
                $result = $this->person;
            }
        }
        catch ( Exception $exception ) {
            //  todo log exception
            throw $exception;
        }

        $this->persons->setPersons( [$result] );

        return $this->persons;
    }

}
