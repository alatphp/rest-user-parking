<?php declare(strict_types=1);

namespace App\Entity;

/**
 * Class Persons
 * @package App\Entity
 */
class Persons
{
    /** @var Person[] */
    private $persons;

    /**
     * @param array $persons
     * @return $this
     */
    public function setPersons( array $persons ): Persons
    {
        $this->persons = $persons;
        return $this;
    }

    /**
     * @param Person $person
     * @return $this
     */
    public function addPersons( Person $person ): Persons
    {
        $this->persons[] = $person;
        return $this;
    }

    /**
     * @return Person|null
     */
    public function getPerson(): ?Person
    {
        return $this->persons[0];
    }

    public function getPersons(): array
    {
        return $this->persons;
    }

}
