<?php declare(strict_types=1);

namespace App\Repository;

use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * Class Person
 * @package App\Repository
 */
class Person extends ServiceEntityRepository
{

    /**
     * Person constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct( ManagerRegistry $registry )
    {
        parent::__construct($registry, \App\Entity\Person::class);
    }

    /**
     * @param array $data
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setOneNewPerson( array $data = [] ): bool
    {
        $person = new \App\Entity\Person();
        try {
            $person->setIdentifier( $data['identifier'] )
                ->setPersonData( $data['person'] )
                ->setClient( $data['client']??'' )
                ->setNew( true )
                ->setCreated( new DateTime( 'now' ) )
                ->setUpdated( new DateTime( 'now' ) );

            $this->getEntityManager()->persist($person);
            $this->getEntityManager()->flush();
        }
        catch ( Exception $exception ) {
            throw $exception;
        }

        return true;
    }
}
