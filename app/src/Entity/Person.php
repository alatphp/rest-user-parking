<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Person
 * @ORM\Entity
 * @package App\Entity
 */
class Person
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=256)
     *
     * @Assert\NotBlank
     */
    private $identifier;

    /**
     * @ORM\Column(type="string", length=256, nullable=false)
     *
     * @Assert\NotBlank
     */
    private $client;

    /**
     * @ORM\Column(type="json", nullable=false)
     *
     * @Assert\Json
     */
    private $person_data;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @ORM\Column( type="boolean", options={"default" : true} )
     */
    private $new;

    /**
     * @return null
     * do not show 'id' in json response
     */
    public function getId()
    {
        return null;
    }

    /**
     * @return int
     */
    private function getInternalId(): int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Person
     */
    public function setId($id): Person
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     * @return $this
     */
    public function setIdentifier( string $identifier ): Person
    {
        $this->identifier = $identifier;
        return $this;
    }

    /**
     * @return string
     */
    public function getClient(): ?string
    {
        return $this->client;
    }

    /**
     * @param string $client
     * @return $this
     */
    public function setClient( string $client ): Person
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPersonData()
    {
        return $this->person_data;
    }

    /**
     * @param mixed $person_data
     * @return Person
     */
    public function setPersonData($person_data): Person
    {
        $this->person_data = $person_data;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): ?\DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return Person
     */
    public function setCreated( \DateTime $created ): Person
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return Person
     */
    public function getUpdated(): ?\DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     * @return $this
     */
    public function setUpdated( \DateTime $updated ): Person
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNew(): ?bool
    {
        return $this->new;
    }

    /**
     * @param bool $new
     * @return bool
     */
    public function setNew( bool $new ): Person
    {
        $this->new = $new;
        return $this;
    }

}
