<?php declare(strict_types=1);

namespace App\Entity;

class HttpInfo
{
    /** @var string */
    private $status;

    /** @var string */
    private $message;

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return HttpInfo
     */
    public function setStatus(string $status): HttpInfo
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $info
     * @return HttpInfo
     */
    public function setMessage(string $info): HttpInfo
    {
        $this->message = $info;
        return $this;
    }
}
