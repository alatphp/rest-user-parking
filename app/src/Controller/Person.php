<?php declare(strict_types=1);

namespace App\Controller;

use App\Service\AppHttpResponse;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class Person
 * @package App\Controller
 */
class Person
{

    /**
     * @param \App\Repository\Person $person
     * @param AppHttpResponse $appHttpResponse
     * @param SerializerInterface $serializer
     */
    public function __construct( private \App\Repository\Person $personRepository,
                                private AppHttpResponse $appHttpResponse,
                                private readonly EntityManagerInterface $entityManager,
                                private SerializerInterface $serializer )
    { }

    /**
     * @Route( "/persons", name="person_post", methods={"POST"} )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function setOnePerson( Request $request ): JsonResponse
    {
        $data = json_decode( $request->getContent(), true );

        try {
            $this->personRepository->setOneNewPerson( $data );
        }
        catch ( Exception $exception ) {
            //  let mandatory fields check by db; in the first step, if it fits not, ...
            $this->appHttpResponse->setStatus( 'ko' )
                ->setMessage( $exception->getMessage() );

            return new JsonResponse( $this->serializer->serialize( $this->appHttpResponse->getEntity(), 'json' ),
                Response::HTTP_UNPROCESSABLE_ENTITY );
        }

        return new JsonResponse( ['OK'], Response::HTTP_OK );
    }

    /**
     * @Route( "/persons", name="person_get", methods={"GET"} )
     *
     * @param Request $request
     * @param \App\Service\Person $personService
     * @param ValidatorInterface $validator
     * @return JsonResponse
     */
    public function getPerson( Request $request,
                                \App\Service\Person $personService,
                                ValidatorInterface $validator ): JsonResponse
    {
        try {
            $persons = $personService->getOnePersonByClientAndIdentifier( $request, $this->entityManager, $validator );
        }
        catch ( Exception $exception ) {
            $this->appHttpResponse->setStatus( 'ko' )
                ->setMessage( $exception->getMessage() );

            return new JsonResponse( $this->serializer->serialize( $this->appHttpResponse->getEntity(), 'json' ),
                Response::HTTP_NOT_FOUND, [], true );
        }

        return new JsonResponse( $this->serializer->serialize( $persons->getPerson(),'json' ), Response::HTTP_OK,
            [], true  );
    }

    /**
     * @Route( "/persons/all", name="persons_get", methods={"GET"} )
     *
     * @param Request $request
     * @param \App\Service\Person $personService
     * @param ValidatorInterface $validator
     * @return JsonResponse
     */
    public function getAllPerson( Request $request,
                                \App\Service\Person $personService,
                                ValidatorInterface $validator ): JsonResponse
    {
        try {
            $persons = $personService->getAllPersonByClient( $request, $this->entityManager, $validator );
        }
        catch ( Exception $exception ) {
            $this->appHttpResponse->setStatus( 'ko' )
                ->setMessage( $exception->getMessage() );

            return new JsonResponse( $this->serializer->serialize( $this->appHttpResponse->getEntity(), 'json' ),
                Response::HTTP_NOT_FOUND, [], true  );
        }

        return new JsonResponse( $this->serializer->serialize( $persons->getPersons(), 'json' ),
            Response::HTTP_OK, [], true  );
    }

    /**
     * @Route( "/persons/new", name="persons_new_get", methods={"GET"} )
     *
     * @param Request $request
     * @param \App\Service\Person $personService
     * @return JsonResponse
     */
    public function getNewPerson( Request $request,
                                \App\Service\Person $personService ): JsonResponse
    {
        try {
            $person = $personService->getNewPersonByClient( $request, $this->entityManager );
        }
        catch ( Exception $exception ) {
            $this->appHttpResponse->setStatus( 'ko' )
                ->setMessage( $exception->getMessage() );

            return new JsonResponse( $this->serializer->serialize( $this->appHttpResponse->getEntity(), 'json' ),
                Response::HTTP_NOT_FOUND, [], true );
        }

        return new JsonResponse( $this->serializer->serialize( $person->getPerson(), 'json' ),
            Response::HTTP_OK, [], true  );
    }

}
