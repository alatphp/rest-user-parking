<?php declare(strict_types=1);

namespace App\Service;

use App\Entity\HttpInfo;

class AppHttpResponse
{
    /** @var HttpInfo  */
    private $httpInfo;

    /**
     * AppHttpResponse constructor.
     * @param HttpInfo $httpInfo
     */
    public function __construct( HttpInfo $httpInfo )
    {
        $this->httpInfo = $httpInfo;
        //  $this->httpInfo = new HttpInfo();   //  todo configure this, work against an interface to change it via config
    }

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage( string $message ): AppHttpResponse
    {
        $this->httpInfo->setMessage( $message );
        return $this;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus( string $status ): AppHttpResponse
    {
        $this->httpInfo->setStatus( $status );
        return $this;
    }

    /**
     * @return HttpInfo
     */
    public function getEntity()
    {
        return $this->httpInfo;
    }

}
